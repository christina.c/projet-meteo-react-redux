import React from 'react';
import Forecast from "./Forecast";
import '../App.css';


function App() {
  return (
    <div>
        <Forecast/>
    </div>
  );
}

export default App;
