import React from 'react';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";

class ForecastResult extends React.Component{

    render(){
        return (
            <div className="fluid-container">
            <div className="container-2">
                <div className="left">
                    <img id="cloud"src="https://www.icone-png.com/png/35/35311.png"></img>
                </div>
                <div className="center">
                    <img id="cloud"src="https://previews.123rf.com/images/sn333g/sn333g1512/sn333g151200030/49646744-r%C3%A9sum%C3%A9-ic%C3%B4ne-soleil.jpg"></img>
                </div>
                <div className="right">
                    <p className="description">{this.props.forecast.current.temperature}</p>
                    <p className="description">{this.props.forecast.current.observation_time}</p>   
                </div>
            </div>

        </div> 
        );
    }
}
 
const mapStateToProps = (state /*, ownProps*/) => {
    return {
        forecast: state.forecastReducer.forecast,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({

    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ForecastResult)