import React from 'react';
import ForecastResult from './ForecastResult';
import ForecastTitle from './ForecastTitle';
import ForecastForm from './ForecastForm';

function Forecast() {
  return (
    <div className="parent">
      <div>
          <ForecastTitle />
          <ForecastResult />
      </div>
      <div>
          <ForecastForm />
      </div>
    </div> 
  );
}

export default Forecast;
