import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

class ForecastChoseCity extends React.Component{

    render(){
        return (
            <div>
                <h1>{this.props.forecast.location.name}</h1>
            </div>
            
        );
    }
}

const mapStateToProps = (state /*, ownProps*/) => {
    return {
        forecast: state.forecastReducer.forecast,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({

    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ForecastChoseCity);
