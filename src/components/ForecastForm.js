import React from 'react';
import { bindActionCreators } from 'redux';
import {connect} from "react-redux";
import { fetchForecast } from '../actions/forecastAction';


class ForecastForm extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            'city' : 'Lyon',
        }
    }

    changeCity = (event) => {
        this.setState({
            'city' : event.target.value
        })
    }

    fetchForecast = () => {
        this.props.fetchForecast(this.state.city);
    }

    render(){
        return (
            <div>
                <input onChange={this.changeCity} value={this.state.city} type="text" />
                <button onClick={this.fetchForecast}>Rechercher</button>
            </div>
        );
    }
}

const mapStateToProps = (state /*, ownProps*/) => {
    return {

    }
  }
  
  function mapDispatchToProps(dispatch) {
    return bindActionCreators({
      fetchForecast, 
    }, dispatch)
  }
  
  export default connect(mapStateToProps, mapDispatchToProps)(ForecastForm)
  