
let initialState = {
    forecast: {
      location: {
          name: '',
          country:'',
          region: '',
          lat: '',
          lon: '',
          timezonz_id:'',
          localtime: '',
          localtime_epoch: '',
      },
      current: {
        observation_time: '',
        temperature: '',
        weather_code: '',
        weather_icons: '',
        weather_descriptions: '',
        wind_speed: '',
        wind_degree: '',
        wind_dir: '',
        pressure:'',
        precip: '',
        humidity: '',
        cloudcover: '',
        feelslike: '',
        uv_index: '',
        uv_index: '',
        is_day: ''
      },
    },
  	loader: false
};

export const forecastReducer = (state = initialState, action) => {
  switch (action.type) {
      case 'UPDATE_FORECAST':
          return {
            ...state,
            forecast: action.value
          };
      case 'TOGGLE_LOADER':
          return {
            ...state,
            loader: action.value
          };
      default :
          return state;
  }
};

export default forecastReducer;